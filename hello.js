"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const app = express();
const portNo = 3000;
// /にアクセスがあった時
app.get("/", (req, res, next) => {
    res.send("Hello, World!");
});
// サーバを起動
app.listen(portNo, () => {
    console.log("起動しました", `http://localhsot:${portNo}`);
});
//# sourceMappingURL=hello.js.map