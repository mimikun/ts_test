import * as express from "express";
const app = express();
const portNo = 3000;

// /にアクセスがあった時
app.get("/", (req: any, res: any, next: any) => {
    res.send("Hello, World!");
});

// サーバを起動
app.listen(portNo, () => {
    console.log("起動しました", `http://localhsot:${portNo}`);
});
